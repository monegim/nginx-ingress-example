# nginx-ingress-example

*NOTE*: There are 2 releases for `nginx controller`. One is [community base](https://github.com/kubernetes/ingress-nginx) and another is managed by [nginx](https://github.com/nginxinc/kubernetes-ingress). The former is used in this assignment.

## Prerequisites
1. Kubernetes Cluster(docker-desktop on MAC has been used for the local development), Version: 1.25
1. kubectl, Version: 1.27
1. helm, Version: 3.14.2
    ```bash
    k get nodes -owide
    NAME             STATUS   ROLES           AGE    VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE         KERNEL-VERSION        CONTAINER-RUNTIME
    docker-desktop   Ready    control-plane   267d   v1.25.9   192.168.65.4   <none>        Docker Desktop   5.15.49-linuxkit-pr   docker://24.0.2
    ```
## Steps

1. Pull the helm chart:

    ```bash
    helm pull ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --untar
    ```

1. Create a namespace for installing the nginx ingress:
    ```bash
    kubectl create ns nginx-ingress
    ```

1. Install the ingress controller:
    ```bash
    helm install infra-nginx-ingress-trial . -n nginx-ingress
    ```
    This installs all the required resources including CRDs.
    ```bash
    helm list -n nginx-ingress
    NAME                            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART                  APP VERSION
    infra-nginx-ingress-trial       nginx-ingress   1               2024-03-08 20:53:09.117298 +0330 +0330  deployed        ingress-nginx-4.10.0   1.10.0
    ```
    Check if the pod is running:
    ```bash
    kubectl get po -n nginx-ingress -owide
    NAME                                                              READY   STATUS    RESTARTS   AGE   IP          NODE             NOMINATED NODE   READINESS GATES
    infra-nginx-ingress-trial-ingress-nginx-controller-7549bd6nll6s   1/1     Running   0          40m   10.1.0.62   docker-desktop   <none>           <none>
    ```

1. Change the `class` name from the nginx to the `nginx-trial` in the `values.yaml`:
    ```yaml
    ingressClassResource:
    # -- Name of the ingressClass
    name: nginx-trial
    # -- Is this ingressClass enabled or not
    enabled: true
    # -- Is this the default ingressClass for the cluster
    default: false
    # -- Controller-value of the controller that is processing this ingressClass
    controllerValue: "k8s.io/ingress-nginx"
    # -- Parameters is a link to a custom resource containing additional
    # configuration for the controller. This is optional if the controller
    # does not require extra parameters.
    parameters: {}
    # -- For backwards compatibility with ingress.class annotation, use ingressClass.
    # Algorithm is as follows, first ingressClassName is considered, if not present, controller looks for ingress.class annotation
    ingressClass: nginx-trial
    ```
1. Deploy a sample application. The cafe application is taken from [this](https://github.com/nginxinc/kubernetes-ingress/tree/main/examples/ingress-resources/complete-example) repo.

1. The application is converted to the helm using `helmify`, and is installed using helm:
    ```bash
    helm install cafe --set cafeSecret.tlsCrt=${TLS_CRT} --set cafeSecret.tlsKey=${TLS_KEY} -n cafe .
    ```
    To not keep the secrets in the version control it is applied using `--set`. This create 2 deployments, services and 1 ingress.

1. Considering the second requirement, an annotation is added to the `ingress` type:
    ```yaml
    annotations:
    # use the shared ingress-nginx
        kubernetes.io/ingress.class: "nginx-trial"
    ```

1. Check if the ingress has been installed correctly:
    ```bash
    NAME                CLASS    HOSTS              ADDRESS     PORTS     AGE
    cafe-cafe-ingress   <none>   cafe.example.com   localhost   80, 443   32m
    ```
### Test the ingress
1. Export ip and port of the ingress endpoint:
    ```bash
    export IC_IP=127.0.0.1
    export IC_HTTPS_PORT=443
    ```
1. Check the response using the command for the `coffee` endpoint:
    ```bash
    curl --resolve cafe.example.com:$IC_HTTPS_PORT:$IC_IP https://cafe.example.com:$IC_HTTPS_PORT/coffee --insecure
    ```
    result:
    ```
    PORT/coffee --insecure 
    Server address: 10.1.0.64:8080
    Server name: cafe-coffee-67f64cf599-64q7w
    Date: 08/Mar/2024:18:15:01 +0000
    URI: /coffee
    Request ID: 8cf51cfe88007ff41f066c03dfcae80f
    ```
    and tea:
    ```bash
    curl --resolve cafe.example.com:$IC_HTTPS_PORT:$IC_IP https://cafe.example.com:$IC_HTTPS_PORT/tea --insecure
    ```
    ```
    PORT/tea --insecure
    Server address: 10.1.0.65:8080
    Server name: cafe-tea-6f5b7b6f7-5qxv4
    Date: 08/Mar/2024:18:16:23 +0000
    URI: /tea
    Request ID: 118a351a74f22f48ccfa206d66eabec4
    ```
### Check the log in the ingress

1. get the ingress pod in the `nginx-ingress` namespace:
    ```bash
    k get -n nginx-ingress pod       
    NAME                                                              READY   STATUS    RESTARTS   AGE
    infra-nginx-ingress-trial-ingress-nginx-controller-7549bd6nll6s   1/1     Running   0          54m
    ```
1. Get the log using:
    ```bash
    k logs -f infra-nginx-ingress-trial-ingress-nginx-controller-7549bd6nll6s -n nginx-ingress
    ```
    Here is the result:
    ```bash
    192.168.65.4 - - [08/Mar/2024:17:39:59 +0000] "GET /tea HTTP/2.0" 200 157 "-" "curl/8.4.0" 36 0.003 [cafe-cafe-tea-svc-80] [] 10.1.0.65:8080 157 0.003 200 40036752b620836e521027102270b4b3
    192.168.65.4 - - [08/Mar/2024:17:40:16 +0000] "GET /coffee HTTP/2.0" 200 164 "-" "curl/8.4.0" 38 0.005 [cafe-cafe-coffee-svc-80] [] 10.1.0.64:8080 164 0.004 200 b25ed69a0587c8e6a7b7bdb81f2ac677
    192.168.65.4 - - [08/Mar/2024:17:40:21 +0000] "GET /coffee1 HTTP/2.0" 404 146 "-" "curl/8.4.0" 39 0.001 [upstream-default-backend] [] 127.0.0.1:8181 146 0.001 404 7fe450ab3cc01d33293824bca6bea848
    192.168.65.4 - - [08/Mar/2024:18:15:01 +0000] "GET /coffee HTTP/2.0" 200 164 "-" "curl/8.4.0" 38 0.006 [cafe-cafe-coffee-svc-80] [] 10.1.0.64:8080 164 0.007 200 961939cb9f66cd149630e3175ac52e92
    192.168.65.4 - - [08/Mar/2024:18:16:23 +0000] "GET /tea HTTP/2.0" 200 157 "-" "curl/8.4.0" 36 0.004 [cafe-cafe-tea-svc-80] [] 10.1.0.65:8080 157 0.004 200 b7bbd06998e2b059f2a00292f4132bf6
    ```